<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
  <title>Twitter</title>
</head>
<body>
<div class="container">
  <div class="page-header">
    <h1>Core Twitter Playground</h1>
  </div>
  <div class="jumbotron">
    <h2>@core_dev_'s Latest Tweet</h2>
      <div class="latestTweet">
        <div class="well">
          Latest Tweet Here
        </div>
      </div>
      <h2>Send a new tweet:</h2>
      <form class="newTweet">
        <div class="form-group">
          <input class="form-control" placeholder="Your tweet here..." type="text" name="tweet" />
        </div>
        <button type="submit" class="btn btn-default">Tweet</button>
      </form>
    </div>
  </div>
</body>
</html>
